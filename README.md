# yarn-plugin-apt_build_plugin



## What is this all about

To make it easy to import this production ready yarn plugin into a project for testing only. 

## Future

This repo does not have future and will be deleted as soon as yarn-plugin-apt passes its tests and it is debianize.

## Usage 1

```
## Clone the repo
git clone https://salsa.debian.org/mr.winz/yarn-plugin-apt_build_plugin

## Make sure you set Yarn version to berry
yarnpkg set version berry

## Import it into your project
yarnpkg plugin import ./yarn-plugin-apt_build_plugin/plugin-apt.js
```

## Usage 2

Add debian/tests/test_yarn-plugin-apt to your project to enable testing yarn-plugin-apt via autopkgtest
